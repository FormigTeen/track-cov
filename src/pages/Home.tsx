import React from 'react';
import { 
    IonContent, 
    IonPage 
} from '@ionic/react';
import Wave from 'react-wavify'

const Home: React.FC = () => {
    return (
        <IonPage>
            <IonContent>
                <Status value={55} />
            </IonContent>
        </IonPage>
    );
};

interface StatusProps {
    value: number
}

const Status: React.FC<StatusProps> = ({ value }) => (
    <Wave  style={{height: `${value}vh`, position: 'fixed', bottom: 0, left: 0}}fill='#049372'
        paused={false}
        options={{
            height: 5,
            amplitude: 60,
            speed: 0.15,
            points: 5
        }}
    >
    </Wave>
)

export default Home;
