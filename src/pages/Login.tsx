import React from 'react';
import { Route } from 'react-router-dom';
import { 
    IonContent, 
    IonRouterOutlet,
    IonInput,
    IonIcon,
    IonLabel,
    IonButton,
    IonGrid,
    IonRow,
    IonPage 
} from '@ionic/react';
import Container from '../components/Container';
import { play } from 'ionicons/icons'

const Login: React.FC = () => {
    return (
        <IonPage>
            <IonRouterOutlet >
                <IonContent>
                    <Container>
                        <h1>Tracking COVID-19</h1>
                        <Route path="/login" component={TelForm} exact={true} />
                        <Route path="/login/code" component={CodeForm} exact={true} />
                    </Container>
                </IonContent>
            </IonRouterOutlet>
        </IonPage>
    );
};

const TelForm: React.FC = () => (
    <>
        <IonLabel>Digite o número de Celular</IonLabel>
        <IonInput 
            color="dark" 
            pattern="tel"
            required
            className="ion-margin-top"
            autofocus 
            type="tel"
            name="phone"
            placeholder="(00) 0 0000-0000"
            inputmode="tel" />
        <IonButton 
            className="ion-margin-top" 
            href="login/code"
            shape="round">
            <IonIcon icon={play}></IonIcon>
        </IonButton>
    </>
)

const CodeForm: React.FC = () => (
    <IonGrid >
        <IonRow className="ion-justify-content-center">
            <IonLabel>Digite o código</IonLabel>
        </IonRow>
        <IonRow className="ion-justify-content-center">
            <IonInput 
                color="dark" 
                required
                className="ion-margin-top"
                autofocus 
                name="code"
                placeholder="000000" />
        </IonRow>
        <IonRow className="ion-justify-content-center">
            <IonButton 
                className="ion-margin-top" 
                shape="round">
                Confirmar
            </IonButton>
        </IonRow>
        <IonRow className="ion-justify-content-center">
            <IonButton 
                className="ion-margin-top" 
                shape="round"
                size="small"
                routerDirection="back"
                href="/login"
                color="danger">
                Voltar
            </IonButton>
        </IonRow>
    </IonGrid>
)
export default Login;
